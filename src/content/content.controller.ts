import { Controller, UseGuards } from "@nestjs/common";
import { ApiAcceptedResponse, ApiBadRequestResponse, ApiBearerAuth, ApiForbiddenResponse, ApiNotFoundResponse, ApiTags, ApiUnauthorizedResponse } from "@nestjs/swagger";
import { Crud, CrudController } from "@nestjsx/crud";
import { Content } from "./content.entity";
import { CreateContentDTO } from "./dto/create.content.dto";
import { UpdateContentDTO } from "./dto/update.content.dto";
import { ContentOwnerGuard } from "./guard/content.owner.guard";
import { ContentService } from "./content.service";
import { AuthGuard } from "src/share/auth.guard";

@Crud({
    model: {
        type: Content,
    },
    routes: {
        only: ['createOneBase', 'getOneBase', 'getManyBase', 'updateOneBase', 'deleteOneBase'],
        getOneBase: {
            decorators: [
                ApiAcceptedResponse({ description: 'Returns specified content' }),
                ApiNotFoundResponse({ description: 'Not found' }),
            ],
        },
        getManyBase: {
            decorators: [
                ApiAcceptedResponse({ description: 'Returns all content' }),
            ],
        },
        createOneBase: {
            decorators: [
                UseGuards(AuthGuard),
                ApiAcceptedResponse({ description: 'Creates content' }),
                ApiUnauthorizedResponse({ description: 'Not authenticated' }),
            ],
        },
        updateOneBase: {
            decorators: [
                UseGuards(ContentOwnerGuard),
                ApiAcceptedResponse({ description: 'Updates content' }),
                ApiNotFoundResponse({ description: 'Not found' }),
                ApiForbiddenResponse({ description: 'Not authorized' }),
            ],
        },
        deleteOneBase: {
            decorators: [
                UseGuards(ContentOwnerGuard),
                ApiAcceptedResponse({ description: 'Deletes content' }),
                ApiNotFoundResponse({ description: 'Not found' }),
                ApiForbiddenResponse({ description: 'Not authorized' }),
            ],
        },
    },
    dto: {
        create: CreateContentDTO,
        update: UpdateContentDTO,
    },
})
@ApiTags('Content')
@Controller('contents')
@ApiBearerAuth()
export class ContentController implements CrudController<Content> {
    constructor(public service: ContentService) { }
}