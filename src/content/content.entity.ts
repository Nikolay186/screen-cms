import { Playlist } from 'src/playlist/playlist.entity';
import { PlaylistContent } from 'src/playlist_content/playlist_content.entity';
import { User } from 'src/user/user.entity';
import {
    Column,
    Entity,
    ManyToOne,
    OneToMany,
    PrimaryGeneratedColumn,
} from 'typeorm';

export enum ContentKind {
    Video = 'Video',
    html = 'HTML',
    Audio = 'Audio',
    Image = 'Image',
}

@Entity('Content')
export class Content {
    @PrimaryGeneratedColumn('increment')
    id: string;

    @Column('text')
    name: string;

    @Column({ type: 'enum', enum: ContentKind })
    kind: ContentKind;

    @Column('text')
    url: string;

    @Column('text')
    owner_id: string;

    @OneToMany(() => Playlist, (playlist) => playlist.playlistContent, {
        cascade: true,
        onDelete: 'CASCADE',
    })
    playlistContent: PlaylistContent[];

    @ManyToOne(() => User, (user) => user.contents, {
        cascade: true,
        onDelete: 'CASCADE',
    })
    owner: User;
}