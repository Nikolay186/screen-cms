import { NotFoundException, CanActivate, ExecutionContext, ForbiddenException } from "@nestjs/common";
import { getRepository } from "typeorm";
import { Content } from "../content.entity";
import { User } from "src/user/user.entity";

export class ContentOwnerGuard implements CanActivate {
    async canActivate(context: ExecutionContext): Promise<boolean> {
        const contentRepository = getRepository(Content);
        const userRepository = getRepository(User);
        const request = context.switchToHttp().getRequest();

        const modContent = await contentRepository.findOne({
            where: { id: request.params.id }
        })
        if (!modContent) {
            throw new NotFoundException('Specified content does not exists');
        }

        const owner = await userRepository.findOne({
            where: { id: modContent.owner_id }
        })

        const user = request.user;
        if (user.id == owner.id) {
            return true;
        } else {
            throw new ForbiddenException('You are not the owner of specified content');
        }
    }
}