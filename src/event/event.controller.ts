import { Controller, UseGuards } from "@nestjs/common";
import { ApiAcceptedResponse, ApiBadRequestResponse, ApiBearerAuth, ApiNotFoundResponse, ApiTags, ApiUnauthorizedResponse } from "@nestjs/swagger";
import { Crud, CrudController } from "@nestjsx/crud";
import { AuthGuard } from "src/share/auth.guard";
import { EventDTO } from "./dto/event.dto";
import { Event } from "./event.entity";
import { EventService } from "./event.service";
import { EventOwnerGuard } from "./guard/event.owner.guard";

@Crud({
    model: {
        type: Event,
    },
    routes: {
        only: ['createOneBase', 'getOneBase', 'updateOneBase', 'deleteOneBase'],
        getOneBase: {
            decorators: [
                ApiAcceptedResponse({ description: 'Returns specified event' }),
                ApiNotFoundResponse({ description: 'Not found' }),
            ],
        },
        getManyBase: {
            decorators: [
                ApiAcceptedResponse({ description: 'Returns all events' }),
            ],
        },
        createOneBase: {
            decorators: [
                UseGuards(AuthGuard),
                ApiAcceptedResponse({ description: 'Creates event' }),
                ApiUnauthorizedResponse({ description: 'Not authenticated' }),
            ],
        },
        updateOneBase: {
            decorators: [
                UseGuards(EventOwnerGuard),
                ApiAcceptedResponse({ description: 'Updates event' }),
                ApiNotFoundResponse({ description: 'Not found' }),
                ApiUnauthorizedResponse({ description: 'Not authorized' }),
            ],
        },
        deleteOneBase: {
            decorators: [
                UseGuards(EventOwnerGuard),
                ApiAcceptedResponse({ description: 'Deletes event' }),
                ApiNotFoundResponse({ description: 'Not found' }),
                ApiUnauthorizedResponse({ description: 'Not authorized' }),
            ],
        },
    },
    dto: {
        create: EventDTO,
        update: EventDTO,
    },
})
@ApiTags('Events')
@ApiBearerAuth()
@Controller('events')
export class EventController implements CrudController<Event> {
    constructor(public service: EventService) { }
}