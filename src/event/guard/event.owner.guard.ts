import { NotFoundException, ForbiddenException, CanActivate, ExecutionContext } from "@nestjs/common";
import { getRepository } from "typeorm";
import { Event } from "../event.entity";
import { User } from "src/user/user.entity";

export class EventOwnerGuard implements CanActivate {
    async canActivate(context: ExecutionContext): Promise<boolean> {
        const eventRepository = getRepository(Event);
        const userRepository = getRepository(User);
        const request = context.switchToHttp().getRequest();
        const event_id = request.params.id;

        const modEvent = await eventRepository.findOne({
            where: { id: event_id },
        });
        if (!modEvent) {
            throw new NotFoundException('Specified event does not exists');
        }

        const owner = await userRepository.findOne({
            where: { id: modEvent.owner_id }
        });

        const user = request.user;
        if (user.id == owner.id) {
            return true;
        } else {
            throw new ForbiddenException('You are not the owner of specified event');
        }
    }
}