import { Controller, UseGuards } from "@nestjs/common";
import { ApiAcceptedResponse, ApiBadRequestResponse, ApiBearerAuth, ApiNotFoundResponse, ApiTags, ApiUnauthorizedResponse } from "@nestjs/swagger";
import { Crud, CrudController } from "@nestjsx/crud";
import { PlaylistService } from "./playlist.service";
import { PlaylistOwnerGuard } from "./guard/playlist.owner.guard";
import { Playlist } from "./playlist.entity";
import { CreatePlaylistDTO } from "./dto/create.playlist.dto";
import { UpdatePlaylistDTO } from "./dto/update.playlist.dto";
import { AuthGuard } from "src/share/auth.guard";

@Crud({
    model: {
        type: Playlist,
    },
    routes: {
        only: ['createOneBase', 'getOneBase', 'getManyBase', 'updateOneBase', 'deleteOneBase'],
        createOneBase: {
            decorators: [
                UseGuards(AuthGuard),
                ApiAcceptedResponse({ description: 'Creates playlist' }),
                ApiUnauthorizedResponse({ description: 'Not authenticated' }),
            ],
        },
        getOneBase: {
            decorators: [
                ApiAcceptedResponse({ description: 'Returns specified playlist' }),
                ApiNotFoundResponse({ description: 'Not found' })
            ],
        },
        getManyBase: {
            decorators: [
                ApiAcceptedResponse({ description: 'Returns all playlists' })
            ],
        },
        updateOneBase: {
            decorators: [
                UseGuards(PlaylistOwnerGuard),
                UseGuards(AuthGuard),
                ApiAcceptedResponse({ description: 'Playlist updated' }),
                ApiUnauthorizedResponse({ description: 'Not authorized' }),
                ApiNotFoundResponse({ description: 'Not found' }),
            ],
        },
        deleteOneBase: {
            decorators: [
                UseGuards(PlaylistOwnerGuard),
                UseGuards(AuthGuard),
                ApiAcceptedResponse({ description: 'Deletes playlist' }),
                ApiNotFoundResponse({ description: 'Not found' }),
                ApiUnauthorizedResponse({ description: 'Not authorized' }),
            ],
        },
    },
    dto: {
        create: CreatePlaylistDTO,
        update: UpdatePlaylistDTO,
    },
})
@ApiTags('Playlists')
@ApiBearerAuth()
@Controller('playlists')
export class PlaylistController implements CrudController<Playlist> {
    constructor(public service: PlaylistService) { }
}