import { IsNumber } from "class-validator";
import { ApiProperty } from "@nestjs/swagger";

export class UpdatePlaylistContentDTO {
    @ApiProperty()
    @IsNumber()
    position?: number;

    @ApiProperty()
    @IsNumber()
    duration?: number;
}