import { Controller, UseGuards } from "@nestjs/common";
import { ApiAcceptedResponse, ApiBadRequestResponse, ApiBearerAuth, ApiNotFoundResponse, ApiTags, ApiUnauthorizedResponse } from "@nestjs/swagger";
import { Crud, CrudController } from "@nestjsx/crud";
import { AuthGuard } from "src/share/auth.guard";
import { CreatePlaylistContentDTO } from "./dto/create.playlist_content.dto";
import { UpdatePlaylistContentDTO } from "./dto/update.playlist_content.dto";
import { OwnerGuard } from "./guard/owner.guard";
import { PlaylistContent } from "./playlist_content.entity";
import { PlaylistContentService } from "./playlist_content.service";

@Crud({
    model: {
        type: PlaylistContent,
    },
    routes: {
        only: ['createOneBase', 'getOneBase', 'getManyBase', 'updateOneBase', 'deleteOneBase'],
        createOneBase: {
            decorators: [
                UseGuards(AuthGuard),
                ApiAcceptedResponse({ description: 'Creates content' }),
                ApiUnauthorizedResponse({ description: 'Not authenticated' }),
            ],
        },
        getOneBase: {
            decorators: [
                ApiAcceptedResponse({ description: 'Returns specified content' }),
                ApiNotFoundResponse({ description: 'Content not found' }),
            ],
        },
        getManyBase: {
            decorators: [
                ApiAcceptedResponse({ description: 'Returns all content' }),
            ],
        },
        updateOneBase: {
            decorators: [
                UseGuards(OwnerGuard),
                ApiAcceptedResponse({ description: 'Updates content' }),
                ApiBadRequestResponse({ description: 'Not authorized' }),
                ApiNotFoundResponse({ description: 'Content not found' }),
            ],
        },
        deleteOneBase: {
            decorators: [
                UseGuards(OwnerGuard),
                ApiAcceptedResponse({ description: 'Deletes content' }),
                ApiBadRequestResponse({ description: 'Not authorized' }),
                ApiNotFoundResponse({ description: 'Content not found' }),
            ],
        },
    },
    dto: {
        create: CreatePlaylistContentDTO,
        update: UpdatePlaylistContentDTO,
    },
})
@Controller('playlist-content')
@ApiTags('Playlist content')
@ApiBearerAuth()
export class PlaylistContentController implements CrudController<PlaylistContent> {
    constructor(public service: PlaylistContentService) { }
}