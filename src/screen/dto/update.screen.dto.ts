import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty } from "class-validator";

export class UpdateScreenDTO {
    @ApiProperty()
    @IsNotEmpty()
    name: string;
}