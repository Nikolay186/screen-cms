import { Controller, UseGuards } from "@nestjs/common";
import { ApiAcceptedResponse, ApiUnauthorizedResponse, ApiBearerAuth, ApiNotFoundResponse, ApiTags } from "@nestjs/swagger";
import { Crud, CrudController } from "@nestjsx/crud";
import { CreateScreenDTO } from "./dto/create.screen.dto";
import { UpdateScreenDTO } from "./dto/update.screen.dto";
import { Screen } from "./screen.entity";
import { ScreenService } from "./screen.service";
import { ScreenOwnerGuard } from "./guard/owner.guard";
import { AuthGuard } from "src/share/auth.guard";

@Crud({
    model: {
        type: Screen,
    },
    routes: {
        only: ['createOneBase', 'getOneBase', 'getManyBase', 'updateOneBase', 'deleteOneBase'],
        getOneBase: {
            decorators: [
                ApiAcceptedResponse({ description: 'Returns screen' }),
                ApiNotFoundResponse({ description: 'Screen not found' }),
            ],
        },
        getManyBase: {
            decorators: [
                ApiAcceptedResponse({ description: 'Returns all screens' })
            ],
        },
        createOneBase: {
            decorators: [
                UseGuards(AuthGuard),
                ApiAcceptedResponse({ description: 'Creates screen' }),
                ApiUnauthorizedResponse({ description: 'Not authenticated' }),
            ],
        },
        updateOneBase: {
            decorators: [
                UseGuards(ScreenOwnerGuard),
                ApiAcceptedResponse({ description: 'Updates screen info' }),
                ApiUnauthorizedResponse({ description: 'Not authorized' }),
                ApiNotFoundResponse({ description: 'Screen not found' }),
            ],
        },
        deleteOneBase: {
            decorators: [
                UseGuards(ScreenOwnerGuard),
                ApiAcceptedResponse({ description: 'Deletes screen' }),
                ApiUnauthorizedResponse({ description: 'Not authorized' }),
                ApiNotFoundResponse({ description: 'Screen not found' }),
            ],
        },
    },
    dto: {
        create: CreateScreenDTO,
        update: UpdateScreenDTO,
    },
})
@Controller('screens')
@ApiTags('Screens')
@ApiBearerAuth()
export class ScreenController implements CrudController<Screen> {
    constructor(public service: ScreenService) { }
}