import { Playlist } from 'src/playlist/playlist.entity';
import { Event } from 'src/event/event.entity';
import {
    Column,
    Entity,
    ManyToOne,
    OneToOne,
    PrimaryGeneratedColumn,
} from 'typeorm';

@Entity('Screen')
export class Screen {
    @PrimaryGeneratedColumn('increment')
    id: string;

    @Column('text')
    name: string;

    @Column('text')
    owner_id: string;

    @ManyToOne(() => Event, (event) => event.screens, {
        cascade: true,
        onDelete: 'CASCADE',
    })
    event: Event;

    @OneToOne(() => Playlist, (playlist) => playlist.screen)
    playlist: Playlist;
}