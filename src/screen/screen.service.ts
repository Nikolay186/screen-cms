import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { TypeOrmCrudService } from "@nestjsx/crud-typeorm";
import { Repository } from "typeorm";
import { Screen } from "./screen.entity";

@Injectable()
export class ScreenService extends TypeOrmCrudService<Screen> {
    constructor(@InjectRepository(Screen) screenRepository: Repository<Screen>) {
        super(screenRepository);
    }
}