import { ArgumentMetadata, HttpException, HttpStatus, Injectable, PipeTransform } from "@nestjs/common";
import { validate } from "class-validator";
import { plainToClass } from "class-transformer";
import { UserDTO } from "src/user/dto/user.dto";
import { CreateContentDTO } from "src/content/dto/create.content.dto";
import { UpdateContentDTO } from "src/content/dto/update.content.dto";
import { CreatePlaylistDTO } from "src/playlist/dto/create.playlist.dto";
import { UpdatePlaylistDTO } from "src/playlist/dto/update.playlist.dto";
import { CreateScreenDTO } from "src/screen/dto/create.screen.dto";
import { UpdateScreenDTO } from "src/screen/dto/update.screen.dto";
import { CreatePlaylistContentDTO } from "src/playlist_content/dto/create.playlist_content.dto";
import { UpdatePlaylistContentDTO } from "src/playlist_content/dto/update.playlist_content.dto";
import { EventDTO } from "src/event/dto/event.dto";

@Injectable()
export class ValidationPipe implements PipeTransform {
    async transform(value: any, metadata: ArgumentMetadata) {
        if (value instanceof Object && this.isEmpty(value)) {
            throw new HttpException(
                'Validation failed: empty body',
                HttpStatus.BAD_REQUEST
            );
        }

        const { metatype } = metadata;
        if (!metatype || !this.toValidate(metatype)) {
            return value;
        }

        const obj = plainToClass(metatype, value);
        const errors = await validate(obj);
        if (errors.length > 0) {
            throw new HttpException(
                `Validation failed: ${this.formatError(errors)}`,
                HttpStatus.BAD_REQUEST
            );
        }
    }

    private toValidate(metatype): boolean {
        const types = [String, Boolean, Number, Array, Object,
            UserDTO,
            CreateContentDTO, UpdateContentDTO,
            CreatePlaylistDTO, UpdatePlaylistDTO,
            CreateScreenDTO, UpdateScreenDTO,
            CreatePlaylistContentDTO, UpdatePlaylistContentDTO,
            EventDTO];
        return !types.find(type => metatype === type);
    }

    private formatError(errors: any[]) {
        return errors.map(
            err => {
                for (let prop in err.constraints) {
                    return err.constraints[prop];
                }
            }).join('; ');
    }

    private isEmpty(value: any) {
        if (Object.keys(value).length > 0) {
            return false;
        }
        return true;
    }
}