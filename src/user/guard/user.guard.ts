import { ForbiddenException, NotFoundException, CanActivate, ExecutionContext } from "@nestjs/common";
import { User } from "../user.entity";
import { getRepository } from "typeorm";

export class UserGuard implements CanActivate {
    async canActivate(context: ExecutionContext): Promise<boolean> {
        const request = context.switchToHttp().getRequest();
        const userRepository = getRepository(User);

        const user_id = request.params.id;
        const modUser = await userRepository.findOne({
            where: { id: user_id }
        });
        if (!modUser) {
            throw new NotFoundException('Specified user does not exists');
        }

        const user = request.user;
        if (modUser.id == user.id) {
            return true;
        } else {
            throw new ForbiddenException('Cannot change other users info');
        }
    }
}