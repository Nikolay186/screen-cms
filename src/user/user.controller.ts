import { Controller, UsePipes, UseGuards } from "@nestjs/common";
import { UserService } from "./user.service"
import { UserDTO } from "./dto/user.dto"
import { UpdateUserDTO } from "./dto/update.user.dto";
import { AuthGuard } from "../share/auth.guard"
import { ValidationPipe } from "../share/validation.pipe"
import { ApiTags, ApiNotFoundResponse, ApiAcceptedResponse, ApiCreatedResponse, ApiForbiddenResponse } from "@nestjs/swagger";
import { Crud, CrudController } from "@nestjsx/crud";
import { User } from "./user.entity";
import { UserGuard } from "./guard/user.guard";

@Crud({
    model: {
        type: User,
    },
    dto: {
        create: UserDTO,
        update: UpdateUserDTO,
    },
    routes: {
        only: ['getManyBase', 'getOneBase', 'updateOneBase', 'createOneBase', 'deleteOneBase'],
        getOneBase: {
            decorators: [
                ApiAcceptedResponse({ description: 'Returns specified user' }),
                ApiNotFoundResponse({ description: 'User not found' }),
            ],
        },
        getManyBase: {
            decorators: [
                ApiAcceptedResponse({ description: 'Returns all registered users' }),
            ]
        },
        updateOneBase: {
            decorators: [
                UseGuards(AuthGuard),
                UseGuards(UserGuard),
                UsePipes(ValidationPipe),
                ApiForbiddenResponse({ description: 'Cannot update other users' }),
                ApiAcceptedResponse({ description: 'Updates user params' }),
                ApiNotFoundResponse({ description: 'User not found' }),
            ],
        },
        createOneBase: {
            decorators: [
                UsePipes(ValidationPipe),
                ApiCreatedResponse({ description: 'Register user' }),
            ]
        },
        deleteOneBase: {
            decorators: [
                UseGuards(UserGuard),
                ApiAcceptedResponse({ description: 'Deletes user' }),
                ApiNotFoundResponse({ description: 'User not found' }),
            ]
        }
    },
})
@ApiTags('User controller')
@Controller('users')
export class UserController implements CrudController<User> {
    constructor(public service: UserService) { }
}