import { Content } from 'src/content/content.entity';
import { Event } from 'src/event/event.entity';
import {
    BeforeInsert,
    Column,
    Entity,
    OneToMany,
    PrimaryGeneratedColumn,
} from 'typeorm';
import * as bcrypt from 'bcrypt';
import { UserRO } from './dto/user.ro';
import * as jwt from 'jsonwebtoken';

@Entity('User')
export class User {
    @PrimaryGeneratedColumn('increment')
    id: string;

    @Column({
        type: 'text',
        unique: true,
    })
    email: string;

    @Column({
        type: 'text',
        unique: true,
    })
    username: string;

    @Column('text')
    password: string;

    @OneToMany(() => Event, (event) => event.owner)
    events: Event[];

    @OneToMany(() => Content, (content) => content.owner)
    contents: Content[];


    @BeforeInsert()
    async hashPassword() {
        this.password = await bcrypt.hash(this.password, 10);
    }

    async checkPassword(password: string): Promise<boolean> {
        return await bcrypt.compare(password, this.password);
    }

    toResponseObject(showToken = true): UserRO {
        const { id, username, email, password, token } = this;
        const responseObject: UserRO = {
            id,
            username,
            email,
            password,
        };
        if (showToken) {
            responseObject.token = token;
        }

        return responseObject;
    }

    private get token(): string {
        const { id, email } = this;
        return jwt.sign(
            {
                id,
                email
            },
            process.env.JWT
        );
    }
}